package gexcel

import (
	"errors"
	"fmt"
	"testing"
)

var (
	excelPath string
)

func init() {
	excelPath = "/Users/liupei/Desktop/1.xlsx"
}

func TestImport(t *testing.T) {
	g, err := New(excelPath)
	if err != nil {
		t.Error(excelPath, " import err:", err)
		return
	}
	valid := `{
    "fieldRule": [
		{
			"title":"姓名",
			"alias": "name",
			"rule": "required",
			"message": "required:姓名必须填"
		},
		{
			"title":"年龄",
			"alias": "age",
			"rule": "required",
			"message": "required:年龄必须填"
		},
		{
			"title":"生日",
			"alias": "birth",
			"rule": "required",
			"message": "required:生日必须填"
		},
		{
			"title":"创建时间",
			"alias": "create_time",
			"rule": "required",
			"message": "required:生日必须填"
		}
	],
	"range": {
		"from": 1,
		"to": 0
	} 
}
`
	//g.AddCustomValidator("myFunc", func(val interface{}) bool {
	//	fmt.Println("hh", val)
	//	return false
	//})
	sheetNames := g.GetSheetList()
	fmt.Println("sheetNames", sheetNames)
	if len(sheetNames) <= 0 {
		t.Error(errors.New("sheetName is empty"))
	}
	err = g.Validate(valid, "Sheet1")
	if err != nil {
		t.Error(err)
	}
	rows := g.GetRows(sheetNames[0])
	fmt.Println(rows)
}
